#[macro_use] extern crate hyper;
extern crate notify_rust;
extern crate rustc_serialize;
extern crate toml;
extern crate websocket;

use std::fs::File;
use std::io::prelude::*;

use notify_rust::Notification;

pub mod message;
use message::{Message, Push};

pub mod pushbullet;
use pushbullet::PBClient;

fn main() {
    let mut cfg_file = File::open("config.toml").expect("Could not find config.toml.");
    let mut s = String::new();
    cfg_file.read_to_string(&mut s).expect("Could not read config.toml");
    let cfg = toml::Parser::new(s.as_ref()).parse().unwrap();
    let cfg_pb = cfg.get("pushbullet")
        .expect("Could not find [pushbullet] section in config.")
        .as_table()
        .expect("Config should contain a [pushbullet] section.");
    let token = cfg_pb.get("token")
        .expect("[pushbullet] section should contain 'token'")
        .as_str()
        .expect("'token' should be a string");

    let mut client = PBClient::new(token.as_ref());

    println!("Starting Pushbullet client...");

    println!("{:?}", client.get_user().expect("Could not get user info from server!"));

    for message in client.messages() {
        match message {
            Message::Push(Push::Mirror {
                title,
                body,
                application_name,
                ..
            }) => {
                let title = format!("{}: {}", application_name, title);
                Notification::new()
                    .body(body.as_ref())
                    .summary(title.as_ref())
                    .show()
                    .unwrap();
            }
            _ => {}
        }
    }
}
