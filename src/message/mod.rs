extern crate rustc_serialize;

use rustc_serialize::{json, Decodable, Decoder};

#[derive(Debug)]
pub enum Push {
    Mirror {
        notification_id: i32,
        title: String,
        body: String,
        application_name: String,
        package_name: String,
        icon: String
    },
    Dismissal {
        notification_id: i32
    }
}

#[derive(Debug)]
pub enum Message {
    Nop,
    Push(Push)
}

impl Message {
    pub fn parse(message: &str) -> Option<Message> {
        let json = json::Json::from_str(message).unwrap();
        let mut decoder = json::Decoder::new(json);
        let result : Result<Message, _> = Decodable::decode(&mut decoder);
        result.ok()
    }
}

impl Decodable for Push {
    fn decode<D: Decoder>(d: &mut D) -> Result<Push, D::Error> {
        d.read_struct("root", 0, |d| {
            let type_ : String = try!(d.read_struct_field("type", 0, Decodable::decode));
            match type_.as_ref() {
                "mirror" => {
                    let id = try!(
                        d.read_struct_field("notification_id", 0, Decodable::decode));
                    let title = try!(
                        d.read_struct_field("title", 0, Decodable::decode));
                    let body = try!(
                        d.read_struct_field("body", 0, Decodable::decode));
                    let app_name = try!(
                        d.read_struct_field("application_name", 0, Decodable::decode));
                    let package_name = try!(
                        d.read_struct_field("package_name", 0, Decodable::decode));
                    let icon = try!(
                        d.read_struct_field("icon", 0, Decodable::decode)); 

                    Ok(Push::Mirror {
                        title: title,
                        body: body,
                        application_name: app_name,
                        package_name: package_name,
                        notification_id: id,
                        icon: icon
                    })
                }
                "dismissal" => {
                    let id = try!(
                        d.read_struct_field("notification_id", 0, Decodable::decode));

                    Ok(Push::Dismissal {
                        notification_id: id
                    })
                }
                _ => {
                    let err_msg = format!(
                        "Invalid value for push type: {} (expected 'mirror' or 'dismissal')",
                        type_);
                    Err(d.error(err_msg.as_ref()))
                }
            }
        })
    }
}

impl Decodable for Message {
    fn decode<D: Decoder>(d: &mut D) -> Result<Message, D::Error> {
        d.read_struct("root", 0, |d| {
            let type_ : String = try!(d.read_struct_field("type", 0, Decodable::decode));

            match type_.as_ref() {
                "nop" => Ok(Message::Nop),
                "push" => {
                    d.read_struct_field("push", 0, |d| {
                        Ok(Message::Push(try!(Decodable::decode(d))))
                    })
                },
                _ => {
                    let err_msg = format!(
                        "Invalid value for message type: {}",
                        type_);
                    Err(d.error(err_msg.as_ref()))
                }
            }
        })
    }
}
