extern crate hyper;
extern crate rustc_serialize;
extern crate websocket;

use message::Message;
pub mod user;

use std::io::Read;

use websocket::Sender;
use websocket::Receiver;
use websocket::WebSocketStream;
use websocket::DataFrame;
use websocket::Client;
use websocket::client::request::Url;

header! { (AccessToken, "Access-Token") => [String] }

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    RequestError(hyper::Error),
    JsonError(rustc_serialize::json::DecoderError),
}

pub struct PBClient {
    token: String,
    client: websocket::client::Client<websocket::dataframe::DataFrame,
                                      websocket::client::sender::Sender<websocket::stream::WebSocketStream>,
                                      websocket::client::receiver::Receiver<websocket::stream::WebSocketStream>>
}

impl PBClient {
    pub fn new(token: &str) -> PBClient {

        let wss_url = format!("wss://stream.pushbullet.com/websocket/{}", token);
        let url = Url::parse(wss_url.as_ref()).unwrap();
        let request = Client::connect(url).unwrap();
        let response = request.send().unwrap();
        response.validate().unwrap();

        PBClient {
            token: token.to_string(),
            client: response.begin()
        }
    }

    fn process_message(message: ::std::result::Result<websocket::Message, websocket::result::WebSocketError>) -> Option<Message> {
        let message = match message {
            Ok(m) => m,
            Err(e) => {
                println!("Error: {:?}", e);
                return None;
            }
        };

        if let websocket::Message::Text(message) = message {
                Message::parse(message.as_ref())
        }
        else {
            None
        }
    }

    pub fn messages<'a>(&'a mut self) -> Box<Iterator<Item=Message> + 'a> {
        let mut receiver = self.client.get_mut_reciever(); // there is a typo in the API
        Box::new(receiver.incoming_messages().filter_map(PBClient::process_message))
    }

    pub fn get_user(&self) -> Result<user::User> {
        let mut client = hyper::Client::new();

        let request = client.get("https://api.pushbullet.com/v2/users/me")
            .header(AccessToken(self.token.clone()))
            .send();

        // TODO: implement From and use try!
        let mut res = match request {
            Ok(r) => r,
            Err(e) => return Err(Error::RequestError(e))
        };

        let mut body = String::new();
        res.read_to_string(&mut body).unwrap();

        match user::User::parse(body.as_ref()) {
            Ok(user) => Ok(user),
            Err(e) => Err(Error::JsonError(e))
        }
    }
}
