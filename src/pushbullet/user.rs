extern crate rustc_serialize;

use rustc_serialize::{json, Decodable, Decoder};

#[derive(RustcDecodable,Debug)]
pub struct User {
    iden: String,
    email: String,
    email_normalized: String,
    name: String,
    image_url: String,
    max_upload_size: i32,
}

impl User {
    pub fn parse(user: &str) -> Result<User, json::DecoderError> {
        let json = try!(json::Json::from_str(user));
        let mut decoder = json::Decoder::new(json);
        Decodable::decode(&mut decoder)
    }
}
